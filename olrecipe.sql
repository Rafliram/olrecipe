-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 11 Feb 2020 pada 07.54
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `olrecipe`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_12_04_133218_tb_user', 1),
(2, '2019_12_04_133541_tb_recipe', 2),
(3, '2019_12_04_133606_tb_archive', 2),
(4, '2019_12_04_133632_tb_like', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_archive`
--

CREATE TABLE `tb_archive` (
  `user_id` int(11) NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `archive_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_archive`
--

INSERT INTO `tb_archive` (`user_id`, `recipe_id`, `archive_id`) VALUES
(14, 5, 16),
(13, 6, 18),
(13, 13, 22);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_histori`
--

CREATE TABLE `tb_histori` (
  `user_id` int(11) NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `created_at` varchar(20) NOT NULL,
  `histori_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_histori`
--

INSERT INTO `tb_histori` (`user_id`, `recipe_id`, `created_at`, `histori_id`) VALUES
(13, 14, '2020-02-04 12:23:13', 5),
(14, 5, '2020-02-04 12:23:20', 6),
(14, 8, '2020-02-04 12:40:28', 7),
(12, 19, '2020-02-11 01:14:31', 8),
(12, 13, '2020-02-11 01:14:43', 9),
(12, 17, '2020-02-11 01:35:46', 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_like`
--

CREATE TABLE `tb_like` (
  `user_id` int(11) NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `like_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_like`
--

INSERT INTO `tb_like` (`user_id`, `recipe_id`, `like_id`) VALUES
(13, 14, 23),
(13, 5, 24),
(13, 6, 25),
(14, 5, 26),
(14, 13, 29),
(14, 13, 30),
(13, 7, 31),
(12, 14, 32);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_recipe`
--

CREATE TABLE `tb_recipe` (
  `recipe_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `kategori` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recipe` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `like` int(11) NOT NULL,
  `isAccept` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_recipe`
--

INSERT INTO `tb_recipe` (`recipe_id`, `user_id`, `kategori`, `title`, `recipe`, `img_url`, `avatar`, `like`, `isAccept`) VALUES
(5, 13, 'Nasgor', 'Nasi goreng rumahan bbbb', 'Bahan:\r\n- Minyak\r\n- Daun bawang 1 batang. Iris halus\r\n- Cabai merah 3 buah\r\n- Daging ayam 125 g. Cincang halus\r\n- Telur 1 butir. Kocok\r\n- Kecap manis 2 sdm\r\n- Bawang merah 5 siung\r\n- Garam 1 sdt\r\n- Merica sdt\r\n- Nasi 600 gr\r\n- Bawang putih 3 siung\r\n\r\nCara memasak:\r\na. Masukkan bawang putih, merah, dan cabai merah ke dalam cobek kemudian haluskan.\r\nb. Goreng telur menjadi orak-arik, sisihkan.\r\nc. Tumis bumbu yang sudah dihaluskan dengan minyak secukupnya hingga harum.\r\nd. Masukkan ayam cincang, telur dan daun bawang ke dalam bumbu. Tumis lagi hingga rata. Tambahkan kecap, garam, dan merica. Aduk hingga rata.\r\ne. Kemudian masukkan nasi dan aduk hingga rata.', 'http://192.168.43.193/lumen/olrecipe/storage/avatar/NASGOR_SIMPLE.jpg', '', 4, 1),
(6, 13, 'Omelette', 'Omelette Telur Ala Hotel', 'Bahan-bahan\n1/2 Butir Bawang Bombay\n2 Butir Telur Ayam\n1 bungkus Sosis Ayam\n1 Wortal\n1 Batang Daun Bawang\n1 Batang Daun Seledri\n25 gram Keju Parut\n1/4 Sdt Merica\nMargarin\nSusu\n\nLangkah	\n- Siapkan bahan-bahan, cuci bersih. Potong kotak kecil2 wortal dan sosis. Iris tipis2 bawang bombay, daun seledri dan daun bawang.\n\n- Kocog telur dan susu 2 sdt secukupnya atau sesuai selera.\n\n- Kemudian lelehkan margarin di teflon, masak sosis hingga matang. Lalu masukkan potongan bawang bombay, wortel, daun seledri dan daun bawang tumis hingga harum. Jangan lupa beri lada dan penyedap rasa sedikit.\n  \n- Setelah itu masukkan telur yg sudah dikocog, ratakan pada wajan tunggu setengah matang, beri taburan keju parut. Masak dengan api kecil agar tdk gosong dan matang merata.\n  \n- Jika sudah matang. Angkat dan Sajikan. Jangan lupa beri topping keju parutnya dan Saus sambal. Yummy', 'http://192.168.43.193/lumen/olrecipe/storage/avatar/OMELETTE.jpg', '', 1, 1),
(12, 13, 'Kue', 'Red velvet cake', 'Bahan-bahan:\n- 250 gr tepung terigu\n- 1/2 sdt baking soda\n- 1 sdt baking powder\n- 1/2 sdt garam\n- 2 sdm cokelat bubuk\n- 1 sdt vanilla extract\n- 125 gr gula kastor\n- 200 ml minyak\n- 2 butir telur\n- 200 ml buttermilk\n- 3/4-1 sdm pewarna merah cabai\n- 1 sdt cuka\n\nCream cheese frosting:\n- 250 gr cream cheese\n- 60 gr unsalted butter\n- 110 gr gula icing\n- 1/2 sdt vanili bubuk\n\nButtermilk:\n- 200 ml susu cair\n- 2 sdm air lemon\n- Campur dan aduk rata diamkan selama 30 menit\n\nCara membuat:\n- Panaskan oven suhu 170 derajat Celcius. Siapkan loyang ukuran 26 cm, olesi dengan margarin dan lapisi dengan baking paper. Sisihkan.\n- Ayak jadi satu bahan kering, tepung terigu, baking soda, baking powder, garam, cokelat bubuk, dan vanili.\n- Dalam wadah masukkan gula kastor, minyak, dan telur. Kocok rata. Masukkan buttermilk dan pewarna merah, kocok rata. Masukkan campuran tepung sedikit demi sedikit, kocok rata. Tambahkan cuka dan kocok rata kembali.\n- Masukkan dalam loyang. Hentakkan sebelum dimasukkan ke oven. Panggang selama 35-40 menit. Lakukan tes tusuk jika tusuk gigi bersih tandanya sudah matang. Dinginkan.\n- Frosting, masukkan semua bahan frosting lalu kocok sampai lembut. Masukkan ke kulkas sebelum digunakan.\n- Penyelesaian, belah dua cake, potong pinggirannya sedikit. Hancurkan potongan cake untuk topping cake, sisihkan. Ambil sebuah cake olesi dengan bahan frosting. Tumpuk dengan cake satunya, olesi dengan bahan frosting. Taburi atasnya dengan remahan cake.\n- Simpan di kulkas sampai set. Rapikan pinggirannya siap disajikan.', 'http://192.168.43.193/lumen/olrecipe/storage/avatar/KUE_MERAH.jpg', '', 0, 1),
(13, 14, 'Kue', 'Bolu gulung cokelat', 'Bahan A:\n- 10 butir kuning telur\n- 4 butir putih telur\n- 100 gr gula pasir\n\nBahan B (diayak):\n- 60 gr terigu protein rendah\n- 20 gr cokelat bubuk\n- 15 gr tepung maizena\n- 1 sdt cake emulsifier/SP/TBM\n\nBahan C:\n- 100 gr mentega, cairkan\n- 1 sdm susu kental manis\n\nFilling:\n- Baileys Buttercream\n\nHiasan:\n- Coklat meses secukupnya\n\nCara membuat:\n- Panaskan oven suhu 180 derajat Celcius, olesi loyang petak ukuran 30 cm dengan margarin dan lapisi baking paper.\n- Mixer bahan A sebentar, tambahkan bahan B mixer sampai benar-benar kental kurang lebih 8 menit.\n- Masukan bahan C mixer cukup sampai tercampur rata jangan over mix, adonan akhir harus masih kental.\n- Tuang ke dalam loyang, panggang selama 20 menit (tergantung oven masing-masing).\n- Keluarkan dari oven, siapkan baking paper balikkan bolu diatasnya, lepaskan pelan-pelan baking paper dan balikkan kembali ke rak kawat dan biarkan dingin. Oleskan buttercream / selai sesuai selera.\n- Gulung sambil dipadatkan dengan bantuan baking paper. Biarkan beberapa saat. Hias sesuai selera dan siap disajikan.', 'http://192.168.43.193/lumen/olrecipe/storage/avatar/KUE_COKLAT.jpg', '', 2, 1),
(14, 13, 'Nasgor', 'Nasi goreng simple ', 'Bahan-bahan\n\n1 piring nasi \n3 siung bawang putih \n4 siung bawang merah \n2 buah cabe rawit \n50 gr Ayam \n50 gr wortel \n1 sdm kecap ikan \n1 sdm kecap Inggris \n1/2 sdt minyak wijen \n1 sdt saos tiram \n1/4 sdt merica \n1 sdt Garam \n1 bks kecap bangau yg 1000 \n1 butir telur \n3 sdm kecap ikan \n1/4 sdt merica\n\nLangkah - langkah\n\nPotong dadu wortel, dan ayam. Cincang halus bawang merah dan putih\n\nTumis bawang sampai harum masukan ayam. Tumis sampai ayam berubah warna lalu masukan wortel tumis lagi sampai layu\n\n\nMasukan nasi. Aduk rata. Tambahkan kecap ikan, kecap Inggris, minyak wijen, saus tiram,merica, garam,dan kecap manis. Koreksi rasa. Lalu terakhir masukan cabe rawit yang sudah dirajang.\n\nScramble egg: beri sedikit minyak di teflon masukan telur aduk sampai set lalu tambahkan minyak ikan dan merica. Jangan beri garam lagi', 'http://192.168.43.193/lumen/olrecipe/storage/avatar/NASGOR.jpg', '', 2, 1),
(15, 13, 'Martabak Manis', 'Martabak Manis (Teflon)', 'Bahan-bahan	\n200 gram terigu segitiga\n1 butir telur\n40 gram gula pasir\n225 ml susu cair\n1 sdt ragi instan\n1 sdt gula pasir\n3 sdm air hangat\n1/4 sdt baking soda\n1/4 sdt garam\nSecukupnya meses, keju, kacang, skm, margarin, gula pasir\n\nLangkah	\nSiapkan bahannya\n \nBuat biang dalam gelas, masukkan air hangat, ragi instan, gula pasir aduk rata tunggu sampai berbuih tanda ragi masih aktif, sisihkan\n\nDalam wadah masukkan gula pasir, telur, baking soda, garam, kocok menggunakan wisk sampai gula larut, tambahkan tepung terigu aduk rata, tuang susu cair secara bertahap aduk rata, tambahkan adonan biang, aduk rata kembali sambil dikocok kocok, diamkan selama 30-60 menit, sampai adonan mengembang\n \nMartabak Manis Teflon langkah memasak 3 foto\nSiapkan cetakan teflon, panaskan, tuang adonan biarkan sampai muncul lubang serat, taburi gula pasir, tutup biarkan sampai matang\n \nAngkat panas panas oles dengan margarin taburi dengan meses, keju, kacang, skm\n\n~ SELAMAT MENIKMATI ~', 'http://192.168.43.193/lumen/olrecipe/storage/avatar/RECIPE_NOOVFNUCDIQX_1581399612.jpg', 'RECIPE_NOOVFNUCDIQX_1581399612.jpg', 0, 0),
(16, 14, 'Martabak Manis', 'Martabak Manis Bangka', 'Bahan-bahan	\n1 tefon 26 cm\nBahan A:\n125 gram tepung terigu serbaguna\n25 gram tepung tapioka\n2 sdm gula pasir\n1/2 sdt garam\n1/4 sdt vanilli bubuk\n1/4 sdt baking powder\n1 sdt coklat bubuk (3 gram)\n1/2 sdt pewarna merah cabe koepoe2\n200 ml air\nBahan B:\n1 butir telur\nBahan C:\n1 sdt soda kue\nTopping:\nSecukupnya margarin\nSecukupnya keju cheddar parut\nSecukupnya kental manis putih\n\nLangkah	\nCampur bahan A,tuang air sedikit demi sedikit sambil diaduk pake whisk sampe tidak ada yang bergerindil\n\nTambahkan telur,aduk rata\n  \nTambahkan soda kue,aduk balik dengan cepat hingga bergelembung\n \nPanaskan teflon sampe berbunyi cess kalo diciprati air langsung hilang airnya.kecilkan api,masukan adonan.goyangkan teflon untuk membentuk pinggiran yang krispi\n  \nMasak sampe bersarang dan permukaanya matang.kalo sudah bersarang boleh ditutup supaya cepat matang.kalo belum bersarang jangan ditutup nanti bisa bantet.diresep asli bawah teflon dialasi seng supaya panasnya merata jadi bagian bawah martabak ngga cepat gosong\n\nKalo sudah matang angkat lalu olesi margarin.taburi parutan keju dan tambahkan kental manis\n \nTangkupkan martabak, olesi margarin lagi lalu potong2 sesuai selera.sajikan', 'http://192.168.43.193/lumen/olrecipe/storage/avatar/RECIPE_VXTPLIU3C4EE_1581400215.jpg', 'RECIPE_VXTPLIU3C4EE_1581400215.jpg', 0, 0),
(17, 14, 'Martabak Telur', 'Martabak Mesir', 'Bahan kulit:\n- 250 gr terigu protein tinggi\n- 160 ml air hangat\n- 5 sdm minyak sayur/minyak goreng\n- 1 sdt garam\n- minyak goreng untuk merendam\n\nBahan isian:\n- 3 sdm minyak samin\n- 300 gr daging sapi, giling\n- 6 butir telur bebek\n- 1 buah bawang bombai ukuran sedang, cincang\n- 3 siung bawang putih, cincang\n- 2 sdt indian curry powder\n- 1/2 sdt lada bubuk\n- 1 sdm gula merah, sisir\n- garam dan gula pasir secukupnya\n- 5 batang daun bawang, iris\n- minyak goreng untuk menumis\n\nCuko martabak:\n- 250 ml air\n- 5 sdm gula merah\n- 1 sdm gula pasir\n- 1/2 sdm garam\n- 2 sdm cuka\n\nAcar martabak:\n- 1/2 buah wortel\n- 1/2 buah timun\n- 6 butir bawang merah\n- 10 buah cabai rawit, biarkan utuh\n- 1 sdm cuka\n- 4 sdm gula pasir\n- garam sejumput\n\nCara membuat:\nKulit:\n- Campur terigu, garam, minyak dan air, aduk rata dan uleni sampai kalis. Bagi adonan jadi 4 bagian sama besar, taruh di bowl, rendam dengan minyak goreng sampai semuanya terendam. Diamkan kurang lebih 2 jam\n\nIsian:\n- Panaskan minyak, tumis bawang bombai, bawang putih hingga harum. Masukkan daging giling. Beri air secukupnya. Tambahkan bubuk kari dan bumbu-bumbu lainnya. Koreksi rasa. Masak sampai daging matang dan agak mengering. Matikan apinya\n- Masukkan daun bawang dan aduk rata\n\nCuko:\n- Rebus semua bahan cuko sampai mendidih. Koreksi rasa. Jika kurang asam boleh tambahkan cuka\n\nAcar:\n- Semua bahan di potong dadu. Aduk sampai semua tercampur rata. Masukkan dalam kulkas minimal 1 jam\n\nPenyelesaian:\n- Telur bebek dikocok lepas. Campur dengan isiannya tadi dan bagi jadi 4 bagian\n- Ambil adonan kulit, pipihkan langsung ditelfon sampai benar-benar tipis dan tidak robek\n- Nyalakan api kecil, beri isian, lipat semua sisi seperti amplop. Matikan api.\n- Sajikan.', 'http://192.168.43.193/lumen/olrecipe/storage/avatar/RECIPE_PUNBLYT53MVI_1581400721.jpg', 'RECIPE_PUNBLYT53MVI_1581400721.jpg', 0, 2),
(18, 14, 'Martabak Telur', 'Martabak Telur Mantap', 'Bahan kulit:\n- 200 gr tepung terigu protein tinggi\n- 140 ml air\n- 30 gr minyak goreng\n- 1/4 sdt garam\n- secukupnya minyak goreng untuk rendaman\n\nBahan isian:\n- 4 butir telur bebek atau telur ayam\n- 3 batang daun bawang ukuran besar, iris\n- 300 gr daging sapi cincang\n- 3 siung bawang putih, geprek cincang\n- 1/2 buah bawang bombai ukuran besar, iris memanjang\n- 1 1/4 sdt garam\n- 1 sdt lada halus\n- 2 sdt gula pasir\n- 1/4 sdt pala bubuk\n- 4 sdm minyak untuk menumis\n- secukupnya minyak untuk menggoreng\n\nCara buat:\nBahan kulit\n- Campurkan terigu dan garam, tambahkan air sedikit demi sedikit sambil diuleni. tambahkan minyak, uleni kembali. setelah rata, bagi adonan menjadi 4 buah (sekitar 80-85 gr), rendam dalam minyak yang banyak selama kurang lebih 1 jam, sisihkan\n\nIsian:\n- Panaskan minyak, tumis bawang putih dan bawang Bombai, tumis sampai agak harum Panaskan 3 sdm minyak, masukkan bawang putih dan bawang bombai, tumis sampai agak harum, masukkan daging, tumis sampai matang, sisihkan\n- Tambahkan telur, daun bawang, garam, lada, gula pasir dan pala bubuk, kocok sampai rata. Sisihkan\n- Panaskan wajan ceper dengan minyak secukupnya dengan api kecil.\n- Ambil 1 buah adonan, dialas yang sudah dilumuri minyak, tekan-tekan adonan sampai agak tipis (hati-hati jangan sa,pai sobek), angkat adonan langsung ke wajan, beri 1/4 adonan isian, lipat perlahan, masak dengan api kecil menuju sedang sampai kuning kecoklatan, angkat, potong-potong, dan sajikan.', 'http://192.168.43.193/lumen/olrecipe/storage/avatar/RECIPE_JU3UI61R1LIX_1581400827.jpg', 'RECIPE_JU3UI61R1LIX_1581400827.jpg', 0, 0),
(19, 14, 'Nugget', 'Nugget Tahu Ayam', 'Bahan-bahan:\n\n7 potong tahu sekumpul\n4 siung bawang putih\n2 buah wortel\n2 daun bawang\ndaging ayam halus\n2 butir telur\ntepung terigu dan tepung panir\nCara membuat:\n\nHaluskan tahu, bawang putih, dan wortel.\n\nIris tipis daun bawang dan daun seledri.\n\nKocok telur.\n\nMasukkan semua bahan dan aduk sampai rata, kemudian kukus selama 30 menit.\n\nSetelah matang dinginkan dan potong sesuai selera.\n\nSiapkan tepung terigu yang sudah diberi air sebagai bahan celupan sebelum digulingkan di tepung panir.\n\nDiamkan di kulkas sebelum menggoreng, panaskan minyak goreng untuk menggoreng nugget tahu.\n\nNugget tahu ayam siap di sajikan.', 'http://192.168.43.193/lumen/olrecipe/storage/avatar/RECIPE_JQX05KBLLISA_1581401414.jpg', 'RECIPE_JQX05KBLLISA_1581401414.jpg', 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profil` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`user_id`, `username`, `email`, `password`, `profil`, `avatar`, `status`) VALUES
(12, 'admin', 'admin@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'http://192.168.43.193/lumen/olrecipe/storage/avatar/PROFIL_OT4AANU2XGCI_1579573168.jpg', 'PROFIL_OT4AANU2XGCI_1579573168', 1),
(13, 'user', 'user@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'http://192.168.43.193/lumen/olrecipe/storage/avatar/PROFILSVGGZLGYSN2V_1580791088.jpg', 'PROFILSVGGZLGYSN2V_1580791088.jpg', 0),
(14, 'Rafliram', 'rafli@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'http://192.168.43.193/lumen/olrecipe/storage/avatar/PROFIL_PCPVCBEDIP1L_1581399845.jpg', 'PROFIL_PCPVCBEDIP1L_1581399845.jpg', 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_archive`
--
ALTER TABLE `tb_archive`
  ADD PRIMARY KEY (`archive_id`);

--
-- Indeks untuk tabel `tb_histori`
--
ALTER TABLE `tb_histori`
  ADD PRIMARY KEY (`histori_id`);

--
-- Indeks untuk tabel `tb_like`
--
ALTER TABLE `tb_like`
  ADD PRIMARY KEY (`like_id`);

--
-- Indeks untuk tabel `tb_recipe`
--
ALTER TABLE `tb_recipe`
  ADD PRIMARY KEY (`recipe_id`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tb_archive`
--
ALTER TABLE `tb_archive`
  MODIFY `archive_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `tb_histori`
--
ALTER TABLE `tb_histori`
  MODIFY `histori_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `tb_like`
--
ALTER TABLE `tb_like`
  MODIFY `like_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT untuk tabel `tb_recipe`
--
ALTER TABLE `tb_recipe`
  MODIFY `recipe_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
