<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Histori; 
use App\Recipe;
use App\User;

class HistoriController extends Controller
{

    public function historiUser($user_id){
        $histori = Histori::where('user_id', $user_id)->orderBy('created_at', 'DESC')->get();

        if($histori){
            $recipe = Recipe::where('isAccept', true)->get();
            $dataHistori = [];
            for($i = 0; $i < count($histori); $i++){
                for($a = 0; $a < count($recipe); $a++){
                    if($histori[$i]->recipe_id == $recipe[$a]->recipe_id){
                        $user = User::where('user_id', $recipe[$a]->user_id)->get()[0];
                        if($user->status != 2){
                            $dataHistori[] = $recipe[$a];
                        }
                    }
                }
            }
            return response()->json([
                'status' => 200,
                'message' => "Histori user",
                'data' => $dataHistori
            ], 200);
        } else {
            return response()->json([
                'status' => 400,
                'message' => "Gagal mendapatkan histori user"
            ], 200);
        }
    }

    public function add(Request $request){
        $user_id = $request->user_id;
        $recipe_id = $request->recipe_id;
        date_default_timezone_set('Asia/Bangkok');
        $date = date('Y-m-d h:i:s');

        $ifExist = Histori::where('user_id', $user_id)->where('recipe_id', $recipe_id)->get();

        if(count($ifExist) != 0){
            $histori = Histori::where('user_id', $user_id)->where('recipe_id', $recipe_id)->update(['created_at' => $date]);
            if($histori){
                return response()->json([
                    'status' => 200,
                    'message' => "Berhasil update histori"
                ], 200);
            } else {
                return response()->json([
                    'status' => 400,
                    'message' => "Gagal update histori"
                ], 200);
            }
        } else {
            $histori = Histori::insert(['user_id' => $user_id, 'recipe_id' => $recipe_id, 'created_at' => $date]);
            if($histori){
                return response()->json([
                    'status' => 200,
                    'message' => "Berhasil menambahkan histori"
                ], 200);
            } else {
                return response()->json([
                    'status' => 400,
                    'message' => "Gagal menambahkan histori"
                ], 200);
            }
        }
    }

    public function deleteHistoriUser($user_id){
        $delete = Histori::where('user_id', $user_id)->delete();

        if($delete){
            return response()->json([
                'status' => 200,
                'message' => 'Berhasil menghapus histori'
            ], 200);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Gagal menghapus histori'
            ], 200);
        }
    }

    public function deleteOne(Request $request){
        $user_id = $request->user_id;
        $recipe_id = $request->recipe_id;

        $delete = Histori::where('user_id', $user_id)->where('recipe_id', $recipe_id)->delete();

        if($delete){
            return response()->json([
                'status' => 200,
                'message' => 'Berhasil menghapus histori'
            ], 200);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Gagal menghapus histori'
            ], 200);
        }
    }
}
