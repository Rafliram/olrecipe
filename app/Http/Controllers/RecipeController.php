<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User; 
use App\Recipe; 

class RecipeController extends Controller
{

    public function ReqRecipe(Request $request){
        $user_id = $request->input('user_id');
        $title = $request->input('title');
        $recipe = $request->input('recipe');
        $kategori = $request->input('kategori');
        $isAccept = false;

        if($request->hasFile('img_url')){
            $img_url = $request->file('img_url');
            $avatar = "RECIPE_" . strtoupper(Str::random(12)) . "_" . time() . '.' . $img_url->getClientOriginalExtension();

            $ReqRecipe = Recipe::insert(['user_id' => $user_id, 'title' => $title, 'recipe' => $recipe, 'img_url' => $this->urlImage($avatar), 'avatar' => $avatar, 'kategori' => $kategori, 'like' => 0, 'isAccept' => $isAccept]);

            if($ReqRecipe){
                $img_url->move(storage_path('avatar'), $avatar);
                return response()->json([
                    'status' => 200,
                    'message' => 'Berhasil mengirim request ke admin' ],200);
            } else {
                return response()->json([
                    'status' => 400,
                    'message' => 'Gagal mengirim request ke admin' ],200);
            }

        } else {
            // $ReqRecipe = Recipe::insert(['user_id' => $user_id, 'title' => $title, 'recipe' => $recipe, 'kategori' => $kategori, 'like' => 0, 'isAccept' => $isAccept]);

            // if($ReqRecipe){
                return response()->json([
                    'status' => 400,
                    'message' => 'Request image file' ],200);
            // }
        }

        // $ReqRecipe = Recipe::insert(['user_id' => $user_id, 'title' => $title, 'recipe' => $recipe, 'img_url' => $img_url, 'kategori' => $kategori, 'isAccept' => $isAccept]);

        // if($ReqRecipe){
        //     return response()->json([
        //         'status' => 200,
        //         'message' => 'Berhasil mengirim request ke admin' ]);
        // }

    }

    public function updateImage(Request $request){
        $recipe_id = $request->input('recipe_id');

        if($request->hasFile('img_url')){
            $img_url = $request->file('img_url');
            $avatar = "RECIPE_" . strtoupper(Str::random(12)) . "_" . time() . '.' . $img_url->getClientOriginalExtension();

            $updateImgage = Recipe::where('recipe_id', $recipe_id)->update(['img_url' => $this->urlImage($avatar)]);

            // $ReqRecipe = Recipe::insert(['user_id' => $user_id, 'title' => $title, 'recipe' => $recipe, 'img_url' => $this->urlImage($avatar), 'avatar' => $avatar, 'kategori' => $kategori, 'like' => 0, 'isAccept' => $isAccept]);

            if($updateImgage){
                $img_url->move(storage_path('avatar'), $avatar);
                return response()->json([
                    'status' => 200,
                    'message' => 'Berhasil mengubah gambar' ],200);
            } else {
                return response()->json([
                    'status' => 400,
                    'message' => 'Gagal mengubah gambar' ],200);
            }

        } else {
            // $ReqRecipe = Recipe::insert(['user_id' => $user_id, 'title' => $title, 'recipe' => $recipe, 'kategori' => $kategori, 'like' => 0, 'isAccept' => $isAccept]);

            // if($ReqRecipe){
                return response()->json([
                    'status' => 400,
                    'message' => 'Request image file' ],200);
            // }
        }
    }

    public function update(Request $request){

        $recipe_id = $request->recipe_id;
        $kategori = $request->kategori;
        $title = $request->title;
        $recipe = $request->recipe;

        $update = Recipe::where('recipe_id', $recipe_id)->update(['kategori' => $kategori, 'title' => $title, 'recipe' => $recipe]);

        // if($update){
            return response()->json([
                'status' => 200,
                'message' => "Berhasil mengubah"], 200);
        // } else {
        //     return response()->json([
        //         'status' => 400,
        //         'message' => "Gagal mengubah"], 200);
        // }

    }

    public function FindRecipe($title){
        $searchRecipe = Recipe::where('title', 'LIKE', "%{$title}%")->orderBy('title', 'ASC')->get();

        if($searchRecipe){
            $recipe = [];

            for($i = 0; $i < count($searchRecipe); $i++){
                $user = User::where('user_id', $searchRecipe[$i]->user_id)->get()[0];
                if($user->status != 2){
                    $recipe[] = $searchRecipe[$i];
                }
            }
            return response()->json([
                'status' => 200,
                'message' => 'Search recipe',
                'data' => $recipe ]);
        }
    }

    public function DelRecipe($recipe_id){
        $delRecipe = Recipe::where('recipe_id', $recipe_id)->delete();

        if($delRecipe){
            return response()->json([
                'status' => 200,
                'message' => 'Berhasil delete recipe' ]);
        }
    }

    public function AcceptedRecipe(){
        $acceptedRecipe = Recipe::where('isAccept', true)->orderBy('title', 'ASC')->get();

        if($acceptedRecipe){
            $recipe = [];

            for($i = 0; $i < count($acceptedRecipe); $i++){
                $user = User::where('user_id', $acceptedRecipe[$i]->user_id)->get()[0];
                if($user->status != 2){
                    $recipe[] = $acceptedRecipe[$i];
                }
            }

            return response()->json([
                'status' => 200,
                'message' => 'Accepted recipe',
                'data' => $recipe ]);
        }

    }

    public function UnAcceptedRecipe(){
        $unAcceptedRecipe = Recipe::where('isAccept', false)->orderBy('title', 'ASC')->get();

        if($unAcceptedRecipe){
            $recipe = [];

            for($i = 0; $i < count($unAcceptedRecipe); $i++){
                $user = User::where('user_id', $unAcceptedRecipe[$i]->user_id)->get()[0];
                if($user->status != 2){
                    $recipe[] = $unAcceptedRecipe[$i];
                }
            }
            return response()->json([
                'status' => 200,
                'message' => 'Unaccepted recipe',
                'data' => $recipe ]);
        }
    }

    public function RejectedRecipe(){
        $rejectedRecipe = Recipe::where('isAccept', 2)->orderBy('title', 'ASC')->get();

        if($rejectedRecipe){
            $recipe = [];

            for($i = 0; $i < count($rejectedRecipe); $i++){
                $user = User::where('user_id', $rejectedRecipe[$i]->user_id)->get()[0];
                if($user->status != 2){
                    $recipe[] = $rejectedRecipe[$i];
                }
            }
            return response()->json([
                'status' => 200,
                'message' => 'Rejected recipe',
                'data' => $recipe ]);
        }

    }

    public function RecipeByUser($user_id){
        // $user = User::where('user_id', $user_id)->get();

        // $recipe = Recipe::where('user_id', $user_id)->where('isAccept', true)->get();

        // if($recipe){
        //     return response()->json([
        //         'user_id' => $user[0]->user_id,
        //         'username' => $user[0]->username,
        //         'email' => $user[0]->email,
        //         'recipe' => $recipe ]);
        // }
        $recipe = Recipe::where('user_id', $user_id)->where('isAccept', true)->orderBy('title', 'ASC')->get();

        if($recipe){
            return response()->json([
                'status' => 200,
                'message' => 'Recipe by user id: ' . $user_id,
                'data' => $recipe ]);
        }
    }

    public function Kategori(){
        $kategori = Recipe::select('kategori')->orderBy('kategori', 'ASC')->distinct()->get();

        if($kategori){
            return response()->json([
                'status' => 200,
                'message' => 'Kategori recipe',
                'data' => $kategori ]);
        }
    }

    public function RecipeByKategori($kategori){
        
        $recipe = Recipe::where('kategori', $kategori)->where('isAccept', true)->orderBy('title', 'ASC')->get();

        if($recipe){
            $recipes = [];

            for($i = 0; $i < count($recipe); $i++){
                $user = User::where('user_id', $recipe[$i]->user_id)->get()[0];
                if($user->status != 2){
                    $recipes[] = $recipe[$i];
                }
            }
            return response()->json([
                'status' => 200,
                'message' => 'Recipe kategori ' . $kategori,
                'data' => $recipes ]);
        }

    }

    public function RecipeById($recipe_id){
        $recipe = Recipe::where('recipe_id', $recipe_id)->get();

        if($recipe){
            return response()->json([
                'status' => 200,
                'message' => 'Recipe dengan id: ' . $recipe_id,
                'data' => $recipe[0] ]);
        }
    }

    public function ConfirmRecipe($recipe_id){
        $recipe = Recipe::where('recipe_id', $recipe_id)->update(['isAccept' => true]);

        if($recipe){
            return response()->json([
                'status' => 200,
                'message' => 'Berhasil confirm recipe dengan id: ' . $recipe_id ], 200);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Gagal confirm recipe dengan id: ' . $recipe_id ], 200);
        }
    }

    public function RejectRecipe($recipe_id){
        $recipe = Recipe::where('recipe_id', $recipe_id)->update(['isAccept' => 2]);

        if($recipe){
            return response()->json([
                'status' => 200,
                'message' => 'Berhasil reject recipe dengan id: ' . $recipe_id ], 200);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Gagal reject recipe dengan id: ' . $recipe_id ], 200);
        }
    }

    public function trendsRecipe(){
        // $trend = [];

        $recipe = Recipe::where('isAccept', true)->where('like', '>', 0)->orderBy('like', 'DESC')->limit(10)->get();

        // foreach($recipe as $trends){
        //     if($trends->like != 0){
        //         $trend[] = $trends;
        //     }
        // }

        // usort($trend, function($a, $b){ return $a->like < $b->like; });
        
        $recipes = [];

        for($i = 0; $i < count($recipe); $i++){
            $user = User::where('user_id', $recipe[$i]->user_id)->get()[0];
            if($user->status != 2){
                $recipes[] = $recipe[$i];
            }
        }

        return response()->json([
            'status' => 200,
            'message' => 'Trends recipe',
            'data' => $recipes ]);
    }
    
}
