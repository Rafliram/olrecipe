<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Like; 
use App\Recipe; 
use App\User; 

class LikeController extends Controller
{

    public function LikeRecipe(Request $request){
        $recipe_id = $request->input('recipe_id');
        $user_id = $request->input('user_id');

        $likeRecipe = Like::insert(['recipe_id' => $recipe_id, 'user_id' => $user_id]);
        $plusLike = Recipe::where('recipe_id', $recipe_id)->get('like');
        $plusLike[0]->like += 1;
        $liked = Recipe::where('recipe_id', $recipe_id)->update(['like' => $plusLike[0]->like]);

        if($likeRecipe){
            return response()->json([
                'status' => 200,
                'message' => 'Berhasil like recipe' ]);
        }

    }

    public function LikeRecipeByUser($user_id){
        $like = Like::where('user_id', $user_id)->get();
        $recipe = Recipe::where('isAccept', true)->get();
        $dataLike = [];
        for($i = 0; $i < count($like); $i++){
            for($a = 0; $a < count($recipe); $a++){
                if($like[$i]->recipe_id == $recipe[$a]->recipe_id){
                    $user = User::where('user_id', $recipe[$i]->user_id)->get()[0];
                    if($user->status != 2){
                        $dataLike[$i] = $recipe[$a];
                    }
                }
            }
        }
        if($like && $recipe){
            return response()->json([
                'status' => 200,
                'message' => 'Recipe liked by User id: ' . $user_id,
                'data' => $dataLike ]);
        }
    }

    public function DelLikeRecipe(Request $request){
        $user_id = $request->input('user_id');
        $recipe_id = $request->input('recipe_id');

        $like = Like::where('user_id', $user_id)->where('recipe_id', $recipe_id)->delete();
        $minLike = Recipe::where('recipe_id', $recipe_id)->get('like');
        $minLike[0]->like -= 1;
        $unLike = Recipe::where('recipe_id', $recipe_id)->update(['like' => $minLike[0]->like]);

        if($like){
            return response()->json([
                'status' => 200,
                'message' => 'Berhasil un like' ]);
        }
    }

}
