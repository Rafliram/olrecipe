<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User; 
use App\Recipe; 

class UserController extends Controller
{

    public function SignUp(Request $request){
        $username = $request->input('username');
        $email = $request->input('email');
        $password = md5($request->input('password'));
        $status = $request->input('status');

        if($request->hasFile('profil')){
            
            $searchEmail = User::where('email', $email)->get();

            if(count($searchEmail) != 0){
                return response()->json([
                    'status' => 400,
                    'message' => 'Email sudah digunakan oleh user lain' ], 200);
            }

            $img_url = $request->file('profil');
            $avatar = "PROFIL_" . strtoupper(Str::random(12)) . "_" . time() . '.' . $img_url->getClientOriginalExtension();

            $signUp = User::insert(['username' => $username, 'email' => $email, 'password' => $password, 'profil' => $this->urlImage($avatar), 'avatar' => $avatar, 'status' => $status]);

            if($signUp){
                $img_url->move(storage_path('avatar'), $avatar);
                $searchEmail = User::where('email', $email)->get();

                return response()->json([
                'status' => 200,
                'message' => 'Berhasil mendaftar',
                'data' => $searchEmail[0] ], 200);
            } else {
                return response()->json([
                'status' => 400,
                'message' => 'Gagal mendaftar'], 200);
            }

        } else {
            $searchEmail = User::where('email', $email)->get();

            if(count($searchEmail) != 0){
                return response()->json([
                    'status' => 400,
                    'message' => 'Email sudah digunakan oleh user lain' ], 200);
            }

            $signUp = User::insert(['username' => $username, 'email' => $email, 'password' => $password, 'profil' => $this->urlImage("PROFIL_FIER9DHMDILK_1581389273.png"), 'avatar' => "PROFIL_FIER9DHMDILK_1581389273.png", 'status' => $status]);

            if($signUp){
                $searchEmail = User::where('email', $email)->get();

                return response()->json([
                'status' => 200,
                'message' => 'Berhasil mendaftar',
                'data' => $searchEmail[0] ], 200);
            } else {
                return response()->json([
                'status' => 400,
                'message' => 'Gagal mendaftar'], 200);
            }
            // $ReqRecipe = Recipe::insert(['user_id' => $user_id, 'title' => $title, 'recipe' => $recipe, 'kategori' => $kategori, 'like' => 0, 'isAccept' => $isAccept]);

            // if($ReqRecipe){
                // return response()->json([
                //     'status' => 400,
                //     'message' => 'Request image file' ],200);
            // }
        }

    }

    public function Login(Request $request){
        $email = $request->input('email');
        $password = md5($request->input('password'));

        $searchEmail = User::where('email', $email)->get();

        if(count($searchEmail) == 0){
            return response()->json([
                'status' => 400,
                'message' => 'Email belum terdaftar' ], 200);
        } else {
            if($searchEmail[0]->status == 2){
                return response()->json([
                    'status' => 400,
                    'message' => 'Akun anda telah di baned oleh admin' ], 200);
            } else {
                if ($searchEmail[0]->password != $password){
                    return response()->json([
                        'status' => 400,
                        'message' => 'Password salah' ], 200);
                }
                return response()->json([
                    'status' => 200,
                    'message' => 'Berhasil login',
                    'data' => $searchEmail[0] ], 200);
            }
        }

    }

    public function Users(){

        $users = User::where('status', '!=', 1)->orderBy('username', 'ASC')->get();
        $fixUsers = [];

        for($i = 0; $i < count($users); $i++){
            $recipe = Recipe::where('user_id', $users[$i]->user_id)->get();
            $countLike = 0;

            for ($a = 0; $a < count($recipe); $a++){
                if($recipe[$a]->user_id == $users[$i]->user_id){
                    $countLike += $recipe[$a]->like;
                }
            }

            $fixUsers[] = array(
                "user_id" => $users[$i]->user_id,
                "username" => $users[$i]->username,
                "email" => $users[$i]->email,
                "profil" => $users[$i]->profil,
                "avatar" => $users[$i]->avatar,
                "status" => $users[$i]->status,
                "like" => $countLike
            );
        }
        

        return response()->json([ 
            'status' => 200,
            'message' => 'Berhasil ambil users',
            'data' => $fixUsers ]);
    }

    public function GetUser($user_id){
        
        $user = User::where( 'user_id', $user_id )->get();
        $fixUsers = [];

        for($i = 0; $i < count($user); $i++){
            $recipe = Recipe::where('user_id', $user[$i]->user_id)->get();
            $countLike = 0;

            for ($a = 0; $a < count($recipe); $a++){
                if($recipe[$a]->user_id == $user[$i]->user_id){
                    $countLike += $recipe[$a]->like;
                }
            }

            $fixUsers[] = array(
                "user_id" => $user[$i]->user_id,
                "username" => $user[$i]->username,
                "email" => $user[$i]->email,
                "profil" => $user[$i]->profil,
                "avatar" => $user[$i]->avatar,
                "status" => $user[$i]->status,
                "like" => $countLike
            );
        }

        return response()->json([
            'status' => 200,
            'messae' => 'Berhasil mengambil user dengan id: ' . $user_id,
            'data' => $fixUsers ]);
    }

    public function update(Request $request){

        $user_id = $request->user_id;
        $username = $request->username;
        $email = $request->email;

        $update = User::where('user_id', $user_id)->update(['username' => $username, 'email' => $email]);

        // if($update){
            return response()->json([
                'status' => 200,
                'message' => "Berhasil mengubah"], 200);
        // } else {
        //     return response()->json([
        //         'status' => 400,
        //         'message' => "Gagal mengubah"], 200);
        // }

    }

    public function updateProfil(Request $request){
        $user_id = $request->input('user_id');

        if ($request->hasFile('profil')){
            $img_url = $request->file('profil');
            $avatar = "PROFIL_" . strtoupper(Str::random(12)) . "_" . time() . '.' . $img_url->getClientOriginalExtension();

            $updateImgage = User::where('user_id', $user_id)->update(['profil' => $this->urlImage($avatar)]);

            // $ReqRecipe = Recipe::insert(['user_id' => $user_id, 'title' => $title, 'recipe' => $recipe, 'img_url' => $this->urlImage($avatar), 'avatar' => $avatar, 'kategori' => $kategori, 'like' => 0, 'isAccept' => $isAccept]);

            // if($updateImgage){
                $user = User::where('user_id', $user_id)->get();
                $img_url->move(storage_path('avatar'), $avatar);
                return response()->json([
                    'status' => 200,
                    'message' => 'Berhasil mengubah gambar',
                    'data' => $user ],200);
            // } else {
            //     return response()->json([
            //         'status' => 400,
            //         'message' => 'Gagal mengubah gambar' ],200);
            // }

        } else {
            // $ReqRecipe = Recipe::insert(['user_id' => $user_id, 'title' => $title, 'recipe' => $recipe, 'kategori' => $kategori, 'like' => 0, 'isAccept' => $isAccept]);

            // if($ReqRecipe){
                return response()->json([
                    'status' => 400,
                    'message' => 'Request image file' ],200);
            // }
        }
    }

    public function DeleteUser($user_id){
        $delete = User::where( 'user_id', $user_id )->delete();

        return response()->json([
            'status' => 200,
            'messae' => 'Berhasil menghapus user dengan id: ' . $user_id ]);
    }

    // public function cobaUpdate(Request $request){
    //     $this->validate($request, [
    //         'avatar' => 'required|image'
    //     ]);

    //     // $avatar = Str::random(34);
    //     $image = $request->file('avatar');
    //     $avatar = "PROFIL_" . strtoupper(Str::random(12)) . "_" . time() . '.' . $image->getClientOriginalExtension();
    //     $image->move(storage_path('avatar'), $avatar);

    //     $user_profile = new User;
    //     $user_profile->username = "tes";
    //     $user_profile->email = "email@gmail.com";
    //     $user_profile->password = "12345678";
    //     $user_profile->profil = $this->urlImage($avatar);
    //     $user_profile->avatar = $avatar;
    //     $user_profile->status = 0;
    //     $user_profile->save();
        
    //     $user = User::where('email', "email@gmail.com")->get();

    //     return response()->json([
    //         'status' => 200,
    //         'message' => "Success update user profile.",
    //         'data' => $user
    //     ]);

    // }

    // public function get_avatar($id)
    // {
    //     $user = User::where('user_id', $id)->get();
    //     if (count($user) != 0) {
    //         return $user[0]->profil;
    //     }
    //     $res['success'] = false;
    //     $res['message'] = "Avatar not found";
        
    //     return $res;
    // }
    
    public function BanedUser(){
        $banned = User::where( 'status', 2 )->orderBy('username', 'ASC')->get();

        return response()->json([
            'status' => 200,
            'message' => 'Banned user',
            'data' => $banned ], 200);
    }

    public function BanUser($user_id){
        $ban = User::where('user_id', $user_id)->update(['status' => 2]);

        if($ban){
            return response()->json([
                'status' => 200,
                'message' => 'Berhasil ban user dengan id: ' . $user_id], 200);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Gagal ban user dengan id: ' . $user_id], 200);   
        }
    }

    public function UnbanUser($user_id){
        $ban = User::where('user_id', $user_id)->update(['status' => 0]);

        if($ban){
            return response()->json([
                'status' => 200,
                'message' => 'Berhasil unban user dengan id: ' . $user_id], 200);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Gagal unban user dengan id: ' . $user_id], 200);   
        }
    }

    public function CheckUser($user_id){
        $check = User::where('user_id', $user_id)->get();

        if($check[0]->status == 2){
            return response()->json([
                'status' => 400,
                'message' => 'Akun anda telah di baned oleh admin'], 200);   
        } else {
            return response()->json([
                'status' => 200,
                'message' => 'Akun masih aktif'], 200);   
        }
    }

}
