<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function urlImage($filename){
        return "http://192.168.43.193/lumen/olrecipe/storage/avatar/".$filename;
    }
}
