<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Archive; 
use App\Recipe; 
use App\User;

class ArchiveController extends Controller
{

    public function ArchiveRecipe(Request $request){
        $recipe_id = $request->input('recipe_id');
        $user_id = $request->input('user_id');

        $archiveRecipe = Archive::insert(['recipe_id' => $recipe_id, 'user_id' => $user_id]);

        if($archiveRecipe){
            return response()->json([
                'status' => 200,
                'message' => 'Berhasil menambah ke archive' ]);
        }

    }

    public function ArchiveRecipeByUser($user_id){
        $archive = Archive::where('user_id', $user_id)->get();
        $recipe = Recipe::where('isAccept', true)->get();
        $dataArchive = [];

        for($i = 0; $i < count($archive); $i++){
            for($a = 0; $a < count($recipe); $a++){
                if($archive[$i]->recipe_id == $recipe[$a]->recipe_id){
                    $user = User::where('user_id', $recipe[$i]->user_id)->get()[0];
                    if($user->status != 2){
                        $dataArchive[$i] = $recipe[$a];
                    }
                }
            }
        }
        
        if($archive && $recipe){
            return response()->json([
                'status' => 200,
                'message' => 'Archive by User id: ' . $user_id,
                'data' => $dataArchive ]);
        }
    }

    public function DelArchiveRecipe(Request $request){
        $user_id = $request->input('user_id');
        $recipe_id = $request->input('recipe_id');

        $archive = Archive::where('user_id', $user_id)->where('recipe_id', $recipe_id)->delete();

        if($archive){
            return response()->json([
                'status' => 200,
                'message' => 'Berhasil un arsip' ]);
        }
    }
    
}
