<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
// Users
$router->get('/users', 'UserController@Users');
$router->get('/users/baned', 'UserController@BanedUser');
$router->get('/users/ban/{user_id}', 'UserController@BanUser');
$router->get('/users/unban/{user_id}', 'UserController@UnbanUser');
$router->post('/users/signup', 'UserController@SignUp');
$router->post('/users/login', 'UserController@Login');
$router->get('/users/delete/{user_id}', 'UserController@DeleteUser');
$router->get('/users/{user_id}', 'UserController@GetUser');
$router->get('/users/cek/{user_id}', 'UserController@CheckUser');
$router->post('/users/update', 'UserController@update');
$router->post('/users/update/profil', 'UserController@updateProfil');
// Recipe
$router->post('/recipes/request', 'RecipeController@ReqRecipe');
$router->post('/recipes/update', 'RecipeController@update');
$router->post('/recipes/update/image', 'RecipeController@updateImage');
$router->get('/recipes/trends', 'RecipeController@trendsRecipe');
$router->get('/recipes/search/{title}', 'RecipeController@FindRecipe');
$router->get('/recipes/delete/{recipe_id}', 'RecipeController@DelRecipe');
$router->get('/recipes/accepted', 'RecipeController@AcceptedRecipe');
$router->get('/recipes/unaccepted', 'RecipeController@UnAcceptedRecipe');
$router->get('/recipes/rejected', 'RecipeController@RejectedRecipe');
$router->get('/recipes/users/{user_id}', 'RecipeController@RecipeByUser');
$router->get('/recipes/kategori', 'RecipeController@Kategori');
$router->get('/recipes/kategori/{kategori}', 'RecipeController@RecipeByKategori');
$router->get('/recipes/{recipe_id}', 'RecipeController@RecipeById');
$router->get('/recipes/confirm/{recipe_id}', 'RecipeController@ConfirmRecipe');
$router->get('/recipes/reject/{recipe_id}', 'RecipeController@RejectRecipe');
// Archive
$router->post('/recipes/archive', 'ArchiveController@ArchiveRecipe');
$router->get('/recipes/archive/{user_id}', 'ArchiveController@ArchiveRecipeByUser');
$router->post('/recipes/archive/delete', 'ArchiveController@DelArchiveRecipe');
// Like
$router->post('/recipes/like', 'LikeController@LikeRecipe');
$router->get('/recipes/like/{user_id}', 'LikeController@LikeRecipeByUser');
$router->post('/recipes/like/delete', 'LikeController@DelLikeRecipe');
// Histori
$router->get('/histori/{user_id}', 'HistoriController@historiUser');
$router->post('/histori/add', 'HistoriController@add');
$router->get('/histori/delete/{user_id}', 'HistoriController@deleteHistoriUser');
$router->post('/histori/delete', 'HistoriController@deleteOne');
//Coba
// $router->post('/user/cobaUpdate', 'UserController@cobaUpdate');
// $router->get('/user/avatar/{id}', 'UserController@get_avatar');