<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbRecipe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_recipe', function (Blueprint $table) {
            $table->increments('recipe_id');
            $table->integer('user_id');
            $table->string('kategori', 100);
            $table->string('title', 100);
            $table->text('recipe');
            $table->string('img_url', 100);
            $table->boolean('isAccept');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_recipe');
    }
}
